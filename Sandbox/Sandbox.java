import java.util.*;

public class Sandbox{
	public static void main(String[] args){
		/*
		prog struc:
		1. input data
		2. perform some processing
		3. output the result
		
		
		everything is a CLASS !!!
		class name = file name.class
		
		identifier:
		must start
				unicode
		
		public class Sandbox{
			public static void main(String[] args){
		
			}
		}
		*/
		Scanner RadInput = new Scanner(System.in);
		System.out.println("Enter Radius:");
		
		double PI = 3.14159;
		double radius = RadInput.nextDouble();
		double diameter = radius * 2;
		double area;
		double circumference;
		
		circumference = PI * diameter;
		area = PI * radius * radius;
		System.out.println("\n PI is: " + PI + "\n the radius is: " + radius + "\n the diameter is: " + diameter + "\n the area is: " + area + "\n the circumference is: " + circumference);
		
	}
}